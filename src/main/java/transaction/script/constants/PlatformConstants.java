package transaction.script.constants;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import transaction.script.platform.utils.PropertiesUtil;

public class PlatformConstants {
	static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(PlatformConstants.class);
	static PropertiesUtil properties;
	static Properties properties2;
	static {
		properties = new PropertiesUtil();
		// This is required to load place holders
		properties2 = new Properties();
		try {
			InputStream inputStream = PlatformConstants.class.getClassLoader().getResourceAsStream("platform.properties");
			InputStream inputStream1 = PlatformConstants.class.getClassLoader().getResourceAsStream("platform.properties");
			properties.load(inputStream);
			properties2.load(inputStream1);
		} catch (IOException e) {
			e.printStackTrace();

		}
	}
	public static final String INMEMORY_COMMONDB_HOST=properties.getProperty("inmemory_commondb_host");
	public static final String INMEMORY_COMMONDB_DRIVER = properties.getProperty("inmemory_commondb_driver");
	public static final String INMEMORY_COMMONDB_USER = properties.getProperty("inmemory_commondb_user");
	public static final String INMEMORY_COMMONDB_PASSWORD = properties.getProperty("inmemory_commondb_password");
	public static final String INMEMORY_COMMONDB_URL = properties.getProperty("inmemory_commondb_url");
	public static final String COMMONDATA_SEARCH = properties.getProperty("commondata_search");
	public static final String COMMONDATA_CLOUDFRONT_URL = properties.getProperty("commondata_cloudfront_url");
	public static final String COMMONDATA_S3_URL = properties.getProperty("commondata_s3_url");
}
