package transaction.script.crawlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Companylist {
	public static void main(String[] args) {
		String baseUrl = "https://companylist.org";
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet();
		int rowNumber = 0;
		for (int i = 1; i <=121; i++) {
			try {
				Document document = Jsoup.parse(IOUtils.toString(new URL(
						"https://companylist.org/Spain/Agriculture/" + i+ ".html")));
				TimeUnit.MILLISECONDS.sleep(500);
				int index = 0;
				String address = "", mobile = "", bizName = "", country = "";
				try {
					Document linkDocument = null;
					Elements text = document.getElementsByClass("result-icons");
					for (Element element : text) {
						Row row = sheet.createRow(rowNumber);
						rowNumber++;
						String subLink = element.attr("href");
						try {
							linkDocument = Jsoup.parse(IOUtils.toString(new URL(baseUrl + subLink)));
							//System.exit(0);
						} catch (Exception e1) {
							continue;
						}
						TimeUnit.MILLISECONDS.sleep(500);
						bizName = linkDocument.getElementsByTag("h1").text();
						System.out.println("\n\n" + bizName);
						mobile =linkDocument.getElementsByAttributeValue("itemprop", "telephone").attr("content");
						System.out.println("mobile : "+mobile);
						address =linkDocument.getElementsByAttributeValue("itemprop", "address").attr("content");
						System.out.println("Address :"+address);
						
				row.createCell(0).setCellValue(bizName);
				row.createCell(1).setCellValue(mobile);
				row.createCell(2).setCellValue(address);
				
				index++;
				}
				} catch (IndexOutOfBoundsException e) {
					break;
				}
			}catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (workbook != null) {
			try (FileOutputStream outputStream = new FileOutputStream(new File(
					"/home/mounika/Desktop/Spain_AgriCompaniesList_Data.xlsx"));) {
				workbook.write(outputStream);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}