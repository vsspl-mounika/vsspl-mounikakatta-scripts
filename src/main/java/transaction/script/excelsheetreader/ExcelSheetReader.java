package transaction.script.excelsheetreader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.platform.utils.HsqlDBUtil;

import com.google.gson.Gson;

public class ExcelSheetReader {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExcelSheetReader.class);
	Gson gson = null;
	static Connection hsqldbConn = null;

	public ExcelSheetReader() {
		hsqldbConn = HsqlDBUtil.dbConnection;
		gson = new Gson();
	}

	public void readExcelSheet(String excelSheetPath, String businessTypeId, String mobileTelecomCode) {
		inputParamValidation(excelSheetPath, businessTypeId, mobileTelecomCode);
	}

	public static void main(String[] args) {
		// String mobileTelecomCodes =
		// "+254,+237,+243,+251,+233,+225,+213,+261,+258,+234,+242,+255,+256,+63,+880,+62,+95,+977,+92,+91,+966,+94,+84,+66,+31,+40,+46";
		List<String> mobileTelecomCodes = Collections.unmodifiableList(Arrays.asList("+254", "+237", "+243", "+251", "+233", "+225", "+213", "+261", "+258", "+234", "+242", "+255", "+256", "+63",
				"+880", "+62", "+95", "+977", "+92", "+91", "+966", "+94", "+84", "+66", "+31", "+40", "+46"));
		if (mobileTelecomCodes.contains("+2"))
			System.out.println("success");
		else
			System.out.println("failure");
	}

	private void inputParamValidation(String excelSheetPath, String businessTypeId, String mobileTelecomCode) {
		List<String> mobileTelecomCodes = Collections.unmodifiableList(Arrays.asList("+254", "+237", "+243", "+251", "+233", "+225", "+213", "+261", "+258", "+234", "+242", "+255", "+256", "+63",
				"+880", "+62", "+95", "+977", "+92", "+91", "+966", "+94", "+84", "+66", "+31", "+40", "+46"));
		List<String> businessTypeIds = Collections.unmodifiableList(Arrays.asList("BT000000,BT000001,BT000002,BT000003,BT000006,BT000008,BT000010"));
		if (excelSheetPath == null || excelSheetPath.trim().isEmpty() || businessTypeId == null || businessTypeId.trim().isEmpty() || mobileTelecomCode == null || mobileTelecomCode.trim().isEmpty()) {
			LOGGER.error(" INPUT PARAMETERS LIKE EXCELSHEETPATH, BUSINESSTYPEID, MOBILETELECOMCODE ARE INVALID ");
			System.exit(0);
		} else if (excelSheetPath == null && businessTypeId == null && mobileTelecomCode == null) {
			if (!mobileTelecomCodes.contains(mobileTelecomCode))
				LOGGER.error("INVALID MOBILE TELECOM CODE ");
			else if (!businessTypeIds.contains(businessTypeId))
				LOGGER.error("INVALID BUSINESSTYPE ");
			System.exit(0);
		}
	}

	public String generateDummyNumbers(String mobileTeleComCode) {
		switch (mobileTeleComCode) {
		// mobile Number length 7
		case "+94":

			break;
		// mobile Number length 8
		case "+225":
		case "+95":

			break;
		// mobile Number length 9
		case "+254":
		case "+237":
		case "+243":
		case "+251":
		case "+233":
		case "+213":
		case "+261":
		case "+258":
		case "+242":
		case "+255":
		case "+256":
		case "+966":
		case "+66":
		case "+36":
		case "+31":
		case "+40":
		case "+46":
			//
			break;
		// mobile Number length 10
		case "+84":
		case "+234":
		case "+91":
		case "+977":
		case "+92":
		case "+62":
		case "+63":
		case "+880":
			//
			break;
		}
		return null;
	}

	public long getDummyMobileNocount(String mobileTelecomCode) throws SQLException {
		long count = 0;
		Connection conn = HsqlDBUtil.dbConnection;
		String mobileNoCountQuery = "SELECT USER_COUNT FROM DUMMY_PHONENUM_COUNT WHERE MOBILE_TELECOME_CODE = ? ";
		PreparedStatement preprdStmt = hsqldbConn.prepareStatement(mobileNoCountQuery);
		preprdStmt.setString(1, mobileTelecomCode);
		ResultSet objResultSet = preprdStmt.executeQuery();
		if (objResultSet.next()) {
			count = objResultSet.getInt("USER_COUNT");
		}
		if (!preprdStmt.isClosed())
			preprdStmt.close();
		return count;
	}

	// restrict other than india while registering .............

	public String gen(String mobileTelecomCode, int limit, int mobileNoLength) throws SQLException {
		long count = getDummyMobileNocount(mobileTelecomCode);
		count++;
		String countStr = "" + count;
		int length = (limit - countStr.trim().length());
		for (int i = 0; i <= length; i++) {
			countStr = "0" + countStr;
		}
		int lengthTwo = mobileNoLength - countStr.length();
		String str = "";
		for (int j = 0; j <= lengthTwo; j++) {
			str = "0" + str.trim();
		}
		return str + countStr;
	}

	/*public static void main(String[] args) {
		try {
			System.out.println(new ExcelSheetReader().gen("+91", 7, 10));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
}