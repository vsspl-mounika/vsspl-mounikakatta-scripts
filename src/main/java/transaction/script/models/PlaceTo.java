/*******************************************************************************
 * Copyright (c) 2013 Vasudhaika Software Solutions Pvt Ltd.
 * All rights reserved.
 *  
 * This code is the confidential and proprietary information of   
 * Vasudhaika Software Solutions Pvt Ltd. You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with 
 * Vasudhaika Software Solutions Pvt Ltd.
 *******************************************************************************/
package transaction.script.models;

public class PlaceTo {
	private String placeId;
	private String placeName;
	private String latitude;
	private String longitude;
	private String localityId;
	private String localityName;
	private String districtId;
	private String districtName;
	private String stateId;
	private String stateName;
	private String regionId;
	private String regionName;
	private String countryId;
	private String countryName;
	private String countryShortName;
	private String locationLong;
	private String locationShort;
	private String location_city_id;
	private String pincode;
	private String location_category_id;
	private String market_id;
	private String geo_name_id;
	private String openerpCountryId;
	private String alsoKnownAs;

	public String getAlsoKnownAs() {
		return alsoKnownAs;
	}

	public void setAlsoKnownAs(String alsoKnownAs) {
		this.alsoKnownAs = alsoKnownAs;
	}

	public String getOpenerpCountryId() {
		return openerpCountryId;
	}

	public void setOpenerpCountryId(String openerpCountryId) {
		this.openerpCountryId = openerpCountryId;
	}

	/**
	 * @return the locationLong
	 */
	public String getLocationLong() {
		return locationLong;
	}

	/**
	 * @param locationLong the locationLong to set
	 */
	public void setLocationLong(String locationLong) {
		this.locationLong = locationLong;
	}

	/**
	 * @return the locationShort
	 */
	public String getLocationShort() {
		return locationShort;
	}

	/**
	 * @param locationShort the locationShort to set
	 */
	public void setLocationShort(String locationShort) {
		this.locationShort = locationShort;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the countryShortName
	 */
	public String getCountryShortName() {
		return countryShortName;
	}

	/**
	 * @param countryShortName the countryShortName to set
	 */
	public void setCountryShortName(String countryShortName) {
		this.countryShortName = countryShortName;
	}

	/**
	 * @return the location_city_id
	 */
	public String getLocation_city_id() {
		return location_city_id;
	}

	/**
	 * @param location_city_id the location_city_id to set
	 */
	public void setLocation_city_id(String location_city_id) {
		this.location_city_id = location_city_id;
	}

	/**
	 * @return the pincode
	 */
	public String getPincode() {
		return pincode;
	}

	/**
	 * @param pincode the pincode to set
	 */
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	/**
	 * @return the location_category_id
	 */
	public String getLocation_category_id() {
		return location_category_id;
	}

	/**
	 * @param location_category_id the location_category_id to set
	 */
	public void setLocation_category_id(String location_category_id) {
		this.location_category_id = location_category_id;
	}

	/**
	 * @return the market_id
	 */
	public String getMarket_id() {
		return market_id;
	}

	/**
	 * @param market_id the market_id to set
	 */
	public void setMarket_id(String market_id) {
		this.market_id = market_id;
	}

	/**
	 * @return the placeId
	 */
	public String getPlaceId() {
		return placeId;
	}

	/**
	 * @param placeId the placeId to set
	 */
	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	/**
	 * @return the placeName
	 */
	public String getPlaceName() {
		return placeName;
	}

	/**
	 * @param placeName the placeName to set
	 */
	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	/**
	 * @return the localityId
	 */
	public String getLocalityId() {
		return localityId;
	}

	/**
	 * @param localityId the localityId to set
	 */
	public void setLocalityId(String localityId) {
		this.localityId = localityId;
	}

	/**
	 * @return the localityName
	 */
	public String getLocalityName() {
		return localityName;
	}

	/**
	 * @param localityName the localityName to set
	 */
	public void setLocalityName(String localityName) {
		this.localityName = localityName;
	}

	/**
	 * @return the districtId
	 */
	public String getDistrictId() {
		return districtId;
	}

	/**
	 * @param districtId the districtId to set
	 */
	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}

	/**
	 * @return the districtName
	 */
	public String getDistrictName() {
		return districtName;
	}

	/**
	 * @param districtName the districtName to set
	 */
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	/**
	 * @return the stateId
	 */
	public String getStateId() {
		return stateId;
	}

	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return stateName;
	}

	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * @return the countryId
	 */
	public String getCountryId() {
		return countryId;
	}

	/**
	 * @return the regionId
	 */
	public String getRegionId() {
		return regionId;
	}

	/**
	 * @param regionId the regionId to set
	 */
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	/**
	 * @return the regionName
	 */
	public String getRegionName() {
		return regionName;
	}

	/**
	 * @param regionName the regionName to set
	 */
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * @return the geo_name_id
	 */
	public String getGeo_name_id() {
		return geo_name_id;
	}

	/**
	 * @param geo_name_id the geo_name_id to set
	 */
	public void setGeo_name_id(String geo_name_id) {
		this.geo_name_id = geo_name_id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((placeId == null) ? 0 : placeId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlaceTo other = (PlaceTo) obj;
		if (placeId == null) {
			if (other.placeId != null)
				return false;
		} else if (!placeId.equals(other.placeId))
			return false;
		return true;
	}
}
