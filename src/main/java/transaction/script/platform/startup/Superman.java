package transaction.script.platform.startup;

import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.models.RegistrationTo;
import transaction.script.platform.utils.MongoDBUtil;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class Superman implements Runnable {
	public static final Logger LOGGER = LoggerFactory.getLogger(Superman.class);

	static Superman classObj = new Superman();
	static BlockingQueue<RegistrationTo> queue = new LinkedBlockingQueue<RegistrationTo>();
	private static Client client = ClientBuilder.newClient();
	static String ipAddress = "";
	static Thread thread = null;
	static int numberOfThreads = 4;
	static int initialNumberOfActiveThreads = 0;
	static boolean populatingQueue = false;
	static boolean processData = true;
	static DB db = null;
	static DBCollection collection = null;
	static DBCollection badUsersCollection = null;
	static Gson gson = null;
	static int numberOfUsersRegistered = 0, numberOfUserRegisterationsFailed = 0, numberOfUsersToRegister = 144000;
	static RegistrationTo objRegistrationToForDummyUser = null;
	static long startTime = System.currentTimeMillis();
	static Scanner scanner = null;
	static String mobileNumber = "", firstName = "", businessName = "";
	static {
		try {
			db = MongoDBUtil.getConnection();
			collection = db.getCollection("PreCreatedUsersDataColl");
			//badUsersCollection = db.getCollection("badUsers");
			gson = new Gson();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		try {
			scanner = new Scanner(System.in);
			System.out.println("\n\nPlease enter ip address or domain::");
			while (scanner.hasNext()) {
				ipAddress = scanner.next();
				System.out.println("Please enter number the number of records to process::");
				numberOfUsersToRegister = scanner.nextInt();
				System.out.println("Please enter dummy user first name::");
				firstName = scanner.next();
				System.out.println("Please enter dummy user business name::");
				businessName = scanner.next();
				System.out.println("Please enter dummy user mobile number::");
				mobileNumber = scanner.next();
				break;
			}
			initialNumberOfActiveThreads = Thread.activeCount();
			displayOptions();
			loop: while (scanner.hasNext()) {
				switch (scanner.next()) {
				case "1"://populate queues
					if (Thread.activeCount() - initialNumberOfActiveThreads > 0)
						startTime = System.currentTimeMillis();
					populateQueue();
					break;
				case "2"://spawn threads
					if (queue.size() > 0 && (Thread.activeCount() - initialNumberOfActiveThreads) == 0)
						startTime = System.currentTimeMillis();
					spawnThreads();
					break;
				case "3"://Show current number of threads
					System.out.println("Current thread count is: " + (Thread.activeCount() - initialNumberOfActiveThreads));
					System.out.println("Current number of records in queue are :: " + queue.size());
					break;
				case "4"://Stop program
					System.out.println("Started stopping threads. Current thread count is: " + (Thread.activeCount() - initialNumberOfActiveThreads));
					while (initialNumberOfActiveThreads > Thread.activeCount()) {
						Thread.currentThread().interrupt();
					}
					System.out.println("Stopped all thread's. Current thread count is : " + (Thread.activeCount() - initialNumberOfActiveThreads));
					break loop;
				default:
					System.out.println("Please enter a proper and valid input.");
					break;
				}
				displayOptions();
			}
			scanner.close();
			System.out.println("Abhishek signing off....");
		} catch (Exception e) {
			LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
			e.printStackTrace();
		}
	}

	private static void populateQueue() {
		populatingQueue = true;
		LOGGER.info("Started populating queues");
		DBCursor cursor = collection.find(new BasicDBObject("isPreCreatedRegComplete", false)).sort(new BasicDBObject("_id", 1)).limit(numberOfUsersToRegister);
		int i = 1;
		while (cursor.hasNext()) {
			String data = cursor.next().toString();
			RegistrationTo objRegistrationTo = gson.fromJson(data, RegistrationTo.class);
			queue.offer(objRegistrationTo);
			//numberOfUsersRegistered++;
			System.out.println(i++);
		}
		populatingQueue = false;
		System.out.println("Current queue size is :: " + queue.size());
	}

	@Override
	public void run() {
		while (true && !Thread.currentThread().isInterrupted()) {
			while (populatingQueue) {
				try {
					TimeUnit.SECONDS.sleep(20);
				} catch (InterruptedException e) {
					LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
					e.printStackTrace();
				}
			}
			//System.out.println("Taking elements for " + i + ":::" + Thread.currentThread().getName());
			if (numberOfUsersRegistered > 0 && numberOfUsersRegistered % 100 == 0) {
				LOGGER.info("\n\n\n\nsuperman in work...");
				LOGGER.info(numberOfUsersRegistered + " users registered until now. Time elapsed is :: " + (System.currentTimeMillis() - startTime) + "ms");
			}
			if (queue.size() == 0) {
				//createDummyUser();
				LOGGER.info("\n\n\nsuperman saves the day");
				LOGGER.info("All data is processed.");
				LOGGER.info("Total number of users registered successfully :: " + numberOfUsersRegistered);
				LOGGER.info("Total number of user registrations failed :: " + numberOfUserRegisterationsFailed);
				LOGGER.info("Total time taken to process " + (numberOfUsersRegistered + numberOfUserRegisterationsFailed) + " is :: " + (System.currentTimeMillis() - startTime)
						+ "ms");
			}
			RegistrationTo registrationTo = queue.poll();
			if (registrationTo == null) {
				Thread.currentThread().interrupt();
				break;
			}
			hitPreCreatedUserEndpoint(registrationTo);
			//System.out.println(i + "element is :" + data + ", and thread name is :" + Thread.currentThread().getName());
		}
	}

	/*private static void createDummyUser() {
		LOGGER.info("Inserting dummy user::");
		objRegistrationToForDummyUser.setName(firstName);
		objRegistrationToForDummyUser.setBusinessName(businessName);
		objRegistrationToForDummyUser.setMobileNumber("+91" + mobileNumber);
		objRegistrationToForDummyUser.setIsPreCreatedRegComplete(false);
		objRegistrationToForDummyUser.setPreCreatedRegInfo(null);
		DBObject dbObj = (DBObject) JSON.parse(gson.toJson(objRegistrationToForDummyUser));
		collection.insert(dbObj);
		hitPreCreatedUserEndpoint(objRegistrationToForDummyUser);
		LOGGER.info("Finished inserting dummy user");
	}*/

	private static void hitPreCreatedUserEndpoint(RegistrationTo registrationTo) {
		WebTarget webTarget = client.target("https://www.kalgudi.com/rest/v1/profiles/otpusersignup");
		Invocation.Builder invocationBuilder = webTarget.request();
		Response clientResponse = invocationBuilder.post(Entity.entity(gson.toJson(registrationTo), MediaType.APPLICATION_JSON));
		JSONObject jsonObject = new JSONObject(clientResponse.readEntity(String.class));
		if (jsonObject.get("code").toString().equals("200")) {
			registrationTo.setIsPreCreatedRegComplete(true);
			registrationTo.setPreCreatedRegInfo("Success::" + jsonObject.get("info").toString());
			//DBObject dbObj = (DBObject) JSON.parse(gson.toJson(registrationTo));
			//badUsersCollection.insert(dbObj);
			collection.update(new BasicDBObject("mobileNumber", registrationTo.getMobileNumber()), gson.fromJson(gson.toJson(registrationTo), BasicDBObject.class));
			numberOfUsersRegistered++;
			if (objRegistrationToForDummyUser == null) {
				objRegistrationToForDummyUser = registrationTo;
			}
		} else {
			System.out.println("bad things are happening");
			registrationTo.setIsPreCreatedRegComplete(true);
			registrationTo.setPreCreatedRegInfo("Error::" + jsonObject.get("error").toString());
			//DBObject dbObj = (DBObject) JSON.parse(gson.toJson(registrationTo));
			//badUsersCollection.insert(dbObj);
			collection.update(new BasicDBObject("mobileNumber", registrationTo.getMobileNumber()), gson.fromJson(gson.toJson(registrationTo), BasicDBObject.class));
			numberOfUserRegisterationsFailed++;
		}
	}

	private static void spawnThreads() {
		for (int i = 0; i < numberOfThreads; i++) {
			thread = new Thread(classObj, "TID : " + (i + 1));
			thread.start();
		}
	}

	private static void displayOptions() {
		System.out.println("\n\nPlease select your desired option ::");
		System.out.println("\t1. Start queueing data");
		System.out.println("\t2. Spawn threads and start consuming queue");
		System.out.println("\t3. Display current number of threads and queue size");
		System.out.println("\t4. Stop execution");
	}
}